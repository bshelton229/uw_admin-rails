# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "uw_admin/rails/version"

Gem::Specification.new do |s|
  s.name        = "uw_admin-rails"
  s.version     = UwAdmin::Rails::VERSION
  s.authors     = ["Bryan Shelton"]
  s.email       = ["bryan@sheltonopensolutions.com"]
  s.homepage    = ""
  s.summary     = %q{UW-Madison rails admin assets}
  s.description = %q{UW-Madison rails admin helpers and assets}

  s.rubyforge_project = "uw_admin-rails"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec"
  s.add_runtime_dependency "rails", ">= 3.1"
end
