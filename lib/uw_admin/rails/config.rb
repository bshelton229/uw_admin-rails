module UwAdmin
  module Rails
    class Config
      class << self
        # Simple title configuration
        attr_accessor :title
        attr_accessor :title_link

        # User links getter
        def user_links
          @user_links.nil? ? true : @user_links
        end

        # User links setter
        def user_links=(l)
          @user_links = l ? true : false
        end

        # Additional JS to load
        def load_js
          @load_js || []
        end

        # Additional JS setter
        def load_js=(js)
          @load_js = js.kind_of?(Array) ? js : [js]
        end

        # Additional CSS to load
        def load_css
          @load_css || []
        end

        # Additional CSS setter
        def load_css=(css)
          @load_css = css.kind_of?(Array) ? css : [css]
        end

        def navbar_inverse
          @navbar_inverse || false
        end

        def navbar_inverse=(b)
          @navbar_inverse = b ? true : false
        end
      end
    end
  end
end
