# Helpers
module UwAdmin
  module Rails
    module ViewHelpers
      # Bootstrap icon helper
      def icon(style)
        raw "<i class='icon-#{style}'></i>"
      end

      # Render form errors for a model
      def form_errors(model)
        if model.errors.any?
          raw "<ul>" + model.errors.full_messages.collect {|message| "<li>#{message}</li>" }.join("\n") + "</ul>"
        else
          ''
        end
      end

      # Admin menu helper
      def admin_menu_list(items, ul_class='nav', active_class='active')
        list_items = Array.new
        items.each do |item|
          # If we've set the active bit, use that to deterimine
          # if the menu item is active, otherwise, see if controller
          # is set and matches the current controller
          if not item[:active].nil?
            active = item[:active] ? true : false
          else
            active = ( params[:controller] and (params[:controller] == item[:controller]) )
          end
          link = link_to(item[:title], item[:link])
          open_tag = active ? "<li class=\"#{active_class}\">" : '<li>'
          close_tag = '</li>'
          list_items << "#{open_tag}#{link}#{close_tag}"
        end
        raw "<ul class=\"#{ul_class}\">" + list_items.join("\n") + '</ul>'
      end

      def render_uw_admin_menu
        UwAdmin::Menu.new(uw_admin_menu, :controller => params[:controller]).html
      end

      def uw_admin_menu
        []
      end

      # Helper to access UwAdmin::Rails::Config
      def uw_admin_config
        Config
      end

      # Get the title link
      def uw_admin_title_link
        case Config.title_link
          when Symbol
            send(Config.title_link.to_s)
          when String
            send(Config.title_link)
          else
            '#'
        end
      end

      # Placeholder methods
      def uw_admin_js; end
      def uw_admin_pre_js; end
      def uw_admin_css; end
      def uw_admin_header; end
      def uw_admin_footer; end
    end
  end
end
