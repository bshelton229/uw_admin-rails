require 'uw_admin/menu'
require 'uw_admin/rails/view_helpers'

module UwAdmin
  module Rails
    # Define an engine so Rails loads vendor/assets
    class Engine < ::Rails::Engine
      initializer "uw_admin" do |app|
        # Load view helpers
        ActionView::Base.send :include, ViewHelpers
        # Make sure we precompile the uw_admin assets
        app.config.assets.precompile += %w(uw_admin.js uw_admin.css)
      end
    end
  end
end
