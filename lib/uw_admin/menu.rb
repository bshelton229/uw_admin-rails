module UwAdmin
  class Menu
    # Include the nice link_to and friends helpers in here
    include ActionView::Helpers

    attr_accessor :menu
    attr_accessor :controller
    # This is an attribute used (and required) by the content_for tags
    attr_accessor :output_buffer

    def initialize(menu, opts={})
      @menu = menu
      @controller = opts[:controller]
      @max_depth = opts[:max_depth] || 2
    end

    # Render the menu as HTML
    def html
      render_ul @menu
    end

    # Render each <ul>
    def render_ul(items, level=1)
      # We only go two levels deep with bootstrap dropdowns
      return '' if level > @max_depth

      # Collect the items into an array of <li>
      # tags using content_tag.
      list_items = items.collect do |item|
        # Go to the next item if there is not access
        next if not item[:access].nil? and not item[:access]

        li_classes = []
        li_attributes = []
        li_content = []

        # If we've set the active bit, use that to deterimine
        # if the menu item is active, otherwise, see if controller
        # is set and matches the current controller
        if not item[:active].nil?
          active = read_active(item[:active])
        else
          active = ( @controller and (@controller == item[:controller]) )
        end
        li_classes << 'active' if active

        # Handle children
        if item[:dropdown]
          # children = item[:dropdown] ? render_ul(item[:dropdown], level+1) : ''
          li_content << link_to(raw("#{item[:title]}<b class=\"caret\"></b>"), '#', :class => ['dropdown-toggle'], :data => { :toggle => 'dropdown'} )
          li_content << render_ul(item[:dropdown], level+1)
          li_classes << 'dropdown'
          # Add the link
        else
          # Add a normal link, no drop-down
          li_content << link_to(item[:title], item[:link])
        end

        # Concat the content into the <li>
        content_tag(:li, :class => li_classes) do
          li_content.collect { |c| concat(c) }
        end
      end

      ul_classes = []

      # Add the nav or dropdown class to the ul
      # based on the level
      if level <= 1
        ul_classes << 'nav'
      else
        ul_classes << 'dropdown-menu'
      end

      # Create the <ul> from the items
      content_tag(:ul, :class => ul_classes) do
        list_items.collect { |list_item| concat(list_item) }
      end
    end
    private
      # Determine active state from an expression or a lambda
      def read_active(active)
        out = false
        if active.kind_of?(Proc)
          if active.lambda?
            out = active.call
          else
            out = active.call(@controller)
          end
        else
          out = active ? true : false
        end
        out
      end
  end
end
