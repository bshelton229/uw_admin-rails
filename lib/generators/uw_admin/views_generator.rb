require 'rails/generators'

module UwAdmin
  module Generators
    class ViewsGenerator < ::Rails::Generators::Base
      source_root File.expand_path('../../../../app/views', __FILE__)

      def install_views
        Dir[File.expand_path('../../../../app/views/uw_admin/*', __FILE__)].each do |f|
          name = File.basename(f)
          copy_file "uw_admin/#{name}", "app/views/uw_admin/#{name}"
        end
      end
    end
  end
end
