# UW Admin generator
UwAdmin::Rails.config do |config|
  # Title and title link
  # config.title = 'UwAdmin'
  # config.title_link = :root_path

  # Extra javascript to load
  # config.load_js = %w(admin)

  # Extra css to load
  # config.load_css = %w(admin)

  # Configure the bootstrap inverse color scheme
  # Default is false
  # config.navbar_inverse = true
end
