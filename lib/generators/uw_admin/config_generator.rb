require 'rails/generators'

module UwAdmin
  module Generators
    class InstallGenerator < ::Rails::Generators::Base
      source_root File.expand_path('../templates', __FILE__)

      def copy_config_initializer
        template 'initializers/uw_admin.rb', File.join('config', 'initializers', 'uw_admin.rb')
      end
    end
  end
end
