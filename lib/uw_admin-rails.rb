require "uw_admin/rails/version"
require "uw_admin/rails/config"
if defined? Rails
  require "uw_admin/rails/engine"
  require "generators/uw_admin/config_generator"
  require "generators/uw_admin/views_generator"
end

module UwAdmin
  module Rails
    def self.config
      yield UwAdmin::Rails::Config
    end

    # A dynamic array of all of the CKeditor js and css files
    # def self.tinymce_assets
    #   @@tinymce_assets ||= begin
    #     Dir[File.expand_path('../../vendor/assets/javascripts/tinymce/**/*.{js,css}', __FILE__)].collect do |f|
    #       f.gsub(/.*\/vendor\/assets\/javascripts\//, '').to_s
    #     end
    #   end
    # end
  end
end
