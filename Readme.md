# UwAdmin Rails

A bootstrap based admin layout.

## Installation

Add this line to your application's Gemfile:

    gem 'uw_admin-rails', :git => 'git@bitbucket.org:uwmadison_ucomm/uw_admin-rails.git'

Run the bundle command to install:

    $ bundle

Generators

#### Generate the config/initializers/uw_admin.rb (recommended)

    $ rails generate uw_admin:install

#### Generate the views

You will most likely need to override some partials used by the uw_admin default layout.

    $ rails generate uw_admin:views

This will place the partials used by the layout in app/views/uw_admin/


## Usage

You will now have a 'uw_admin' layout available.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
